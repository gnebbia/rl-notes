# Decision Making

We introduce decision making under uncertainty through the simplified
setup of the k-armed bandit problem.

The bandit problem can be used to describe fundamental concepts
like: rewards, time steps and values.


## k-armed bandit problem

In the k-armed bandit problem we have an agent
who chooses between "k" actions and receives
a reward based on the action it chooses.

We can make an analogy with a doctor which has
to prescribe a medicine to a patient, and has three options
when it comes to medicine.

In this case the doctor is our "agent" which has to learn.  The doctor has
to choose between three different actions (blue, yellow or red treatment).
The welfare of the patient after the treatment is the reward the doctor
receives.

For the doctor to choose which action is the best, we must define the value
of taking each action.
We call these values the action-values or the action value function.


The value is the expected reward:

    q_star(a) = Expected[R_t | A_t = a] foreach a in {1,...,k}
              = Sum( p(r|a) * r )

The goal is to maximize the expected reward:

    argmax(a, q_star(a))

So finding the argument that maximizes our function `q_star`.

## Learning Action Values



## Exploration/Exploitation Tradeoff

Exploration  - improve knowledge for long-term benefit
Exploitation - exploits knowledge for short-term benefit

The exploration/exploitation dilemma states:

    How do we choose when to explore and when to exploit?

A simple method is to use the epsilon-greedy algorithm.

### Epsilon-Greedy Method

One strategy for choosing between exploitation or exploration
is to choose randomly.
We could exploit most of the time with a small chance of exploring.

For example we could have a 1/6 probability of exploring
and 5/6 of choosing the greedy action.
This method is known as epsilon-greedy where epsilon
is the probability used to explore.


Epsilon greedy can be written in the following way:

At =
    argmax(a, Q_t(a))           with probability p = 1 - epsilon
    a ~ Uniform ({a_1,...,a_k}) with probability p = epsilon

### Optimistic Initial Values

Optimistic Initial Values is a technique that encourages early exploration.
Basically what happens here is that in order to encourage
an early exploration, the reward for each action is set to a high value,
so that in case an action is successful our estimate for that action decreases
but in case an action is not successful our estimate for that action
decreases more.

Using optomistic initial values is not the optimal solution
for balancing the exploration vs exploitation dilemma.

Its limitations are:
- optimistic initial values only "drive early exploration":
  this means that agents will not continue exploring after some time;
- not well suited for non-stationary problems:
  for example, one of the action values may change after some number of time steps;
- we may not know what the optimistic initial value should be set to:
  for example, in practice we may not know always the maximum reward;

Generally Optimistic Initial Values is heuristically effective and
is used in combination with other exploration techniques.

### Upper-Confidence Bound (UCB) Action Selection

Another method to balance between exploration and exploitation
is known as Upper-Confidence Bound (UCB) action selection.
UCB uses uncertainty in estimates to drive exploration.

In UCB we choose an action if its confidence interval is larger,
hence we tend to choose actions we know less about. This
algorithm picks the action with the highest upper bound.

    A_t = argmax( Q_t(a) +
                  c*sqrt(ln(t) / N_t(a)))

- `Q_t(a)` represents the exploitation part of the action selection
   process;
- `c*sqrt(ln(t) / N_t(a))` represents the exploration part or the action
   selection process;
- `c` is a user-specified value to control how much
  exploration we perform;
- `t` in ln(t) is the number of timesteps;
- `N_t` is the number of times action `a` is taken;

UCB exploration reduces over time with respect to the epsilon-greedy
techniques that will always takes a random action a certain percentage of times.



